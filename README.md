# KULEČNÍK

## VOSPlzeň, Základy programování, 3. semestr
---
* aplikace ve WinForms, za využítí OOP
* program vygeneruje daný počet kuliček, které se pohybují ve vyhrazeném poli
* po odrazu od stěny se pohybují opačným směrem
* lze nastavit počet a barva kuliček, barva pozadí
